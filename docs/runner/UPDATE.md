# Обновление раннера

## Остановить текущий раннер

```
sudo gitlab-runner stop
```

## Скачать и заменить бинарный файл

```
sudo curl \
  -L \
  --output /usr/local/bin/gitlab-runner \
  "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
```

## Сделать бинарный файл исполняемым

```
sudo chmod +x /usr/local/bin/gitlab-runner
```

## Запустить службу

```
sudo gitlab-runner start
```
