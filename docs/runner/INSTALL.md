# Установка GitLab Runner

## Скачать и поместить бинарник в нужную директорию

```
sudo curl \
  -L \
  --output /usr/local/bin/gitlab-runner \
  "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
```

## Сделать файл исполняемым

```
sudo chmod +x /usr/local/bin/gitlab-runner
```

## Создать пользователя

```
sudo useradd \
  --comment 'GitLab Runner' \
  --create-home gitlab-runner \
  --shell /bin/bash
```

## Установить и запустить бинарник как сервис

```
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
```
