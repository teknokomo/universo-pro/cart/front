# Регистрация раннера

## Запустить интерактивный процесс регистрации

```
sudo gitlab-runner register
```

## Ввести данные для регистрации

```
Enter the GitLab instance URL:
https://gitlab.com/

Enter the registration token:
Скопировать его из GitLab

Enter a description for the runner:
cart_frontend_runner

Enter tags for the runner (comma-separated):
Ничего не вводил

Enter optional maintenance note for the runner:
Ничего не вводил

ВЫБОР ВАРИАНТА

ВАРИАНТ shell
Enter an executor:
shell

ВАРИАНТ docker
Enter an executor:
docker

Enter the default Docker image:
node:20.5.0-alpine3.18

КОНЕЦ ВЫБОРА

Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!

Configuration (with the authentication token) was saved in "/etc/gitlab-runner/config.toml"
```

## Проверить наличие раннера

```
sudo gitlab-runner verify
```
