# CICD

## Установка Gitlab Runner на целевом сервере

Скачать и запустить скрипт установки репозитория для GitLab Runner

```
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
```

Вывод

```
[sudo] пароль для tumanov:
Detected operating system as debian/bullseye.
Checking for curl...
Detected curl...
Checking for gpg...
Detected gpg...
Running apt-get update... done.
Installing debian-archive-keyring which is needed for installing
apt-transport-https on many Debian systems.
Installing apt-transport-https... done.
Installing /etc/apt/sources.list.d/runner_gitlab-runner.list...done.
Importing packagecloud gpg key... done.
Running apt-get update... done.

The repository is setup! You can now install packages.
```

Обновить и установить GitLab Runner

```
sudo apt-get update
sudo apt-get install gitlab-runner
```

### Создание раннера для проекта

Зайти в проект [Фронтенд списков покупок](https://gitlab.com/teknokomo/universo-pro/cart/front/)

Выбрать меню Settings -> CICD

Раскрыть блок Runners

Нажать кнопку "New project runner".

Указать флаг "Run untagged jobs".

Выбрать платформу "Linux".

Нажать кнопку "Create runner".

### Регистрация раннера

Зайти на сервер с раннером

Зарегистрировать его

```
sudo gitlab-runner \
  register \
  --url https://gitlab.com \
  --token $GITLAB_RUNNER_TOKEN
```

Выбрать исполнителем вариант Shell

Перезапустить Runner

```
sudo sysetmctl restart gitlab-runner
```

Проверить статус раннера

```
sudo gitlab-runner verify
```

## Написать сценарий

## Запустить установку
