import { boot } from 'quasar/wrappers';
import { IndexedDBStorage } from 'src/utils/IDBManager';
const idb = new IndexedDBStorage('cartsDB');

// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(async ({ app } /* { app, router, ... } */) => {
  app.config.globalProperties.$idb = idb;

  // something to do
});
export { idb };
