export default {
  failed: 'Action failed',
  success: 'Action was successful',
  Home: 'Home',
  About: 'About',
  Contact: 'Contact',
  Register: 'Register',
  Dashboard: 'Dashboard',
  Settings: 'Settings',
  Profile: 'Profile',
  Профиль: 'Profile',
  language: 'Language',
  Lists: 'Lists',
  Списки: 'Lists',
  Help: 'Help',
  Помощь: 'Help',
  Login: 'Login',
  Logout: 'Logout',
  HelpMessage:
    'Create lists. Fill them with content. Share them with family, friends, and anyone you want.',
  Вход: 'Login',
  Выход: 'Logout',
  HelpPage: {
    HelpMessage:
      'Create lists. Fill them with content. Share them with family, friends, and anyone you want.',
    ToLists: 'To Lists',
  },
  LoginPage: {
    LoginMessage:
      'Access is provided through the Universo project authentication server. It is necessary to be able to create your own shopping lists, both available to everyone and to a limited circle of people.',
    ButtonMessage: 'Log in through Universo',
  },
  PublicCart: {
    NoItems: 'No Items',
    name: 'Item name',
    add: 'Create',
    save: 'Save',
    delete: 'Delete',
    create: 'Create Item',
    remove: 'Delete Item',
    change: 'Change Item',
    deleteMessage: 'Delete Item',
    quantity: 'Quantity',
    messages: {
      msg1: 'You are not authorized',
      msg2: 'Error retrieving list:',
      msg3: 'Unnamed item',
      msg4: 'Item added',
      msg5: 'Error adding item:',
      msg6: 'Checkmark',
      msg7: 'set',
      msg8: 'cleared',
      msg9: 'Error setting checkmark:',
      msg10: 'Item deleted',
      msg11: 'Error deleting item:',
      msg12: 'Unnamed item',
      msg13: 'Item changed',
      msg14: 'Error changing item:',
      msg15: 'Item name cannot be empty',
    },
  },
  PublicCarts: {
    NoLists: 'No Lists',
    name: 'Item name',
    add: 'Create',
    save: 'Save',
    delete: 'Delete',
    create: 'Create List',
    remove: 'Delete List',
    change: 'Change List',
    deleteMessage: 'Delete list',
    messages: {
      msg1: 'You are not authorized',
      msg2: 'Error retrieving lists',
      msg3: 'List name cannot be empty',
      msg4: 'List created',
      msg5: 'Error adding list',
      msg6: 'List name cannot be empty',
      msg7: 'Name changed',
      msg8: 'Error changing list',
      msg9: 'List deleted',
      msg10: 'Error deleting list',
    },
  },
};
