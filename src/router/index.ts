import { debugLog } from '../utils';

debugLog('/src/router/index.js');

import { route } from 'quasar/wrappers';
import {
  createRouter,
  createMemoryHistory,
  createWebHistory,
  createWebHashHistory,
} from 'vue-router';
import routes from './routes';
import { LocalStorage } from 'quasar';
import { useCurrentUserStore } from 'src/stores/current-user';
import { useNetworkStore } from 'src/stores/network';

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route(function (/* { store, ssrContext } */) {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : process.env.VUE_ROUTER_MODE === 'history'
      ? createWebHistory
      : createWebHashHistory;

  const Router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,

    // Leave this as is and make changes in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    // history: createHistory(process.env.MODE === 'ssr' ? void 0 : process.env.VUE_ROUTER_BASE)
    history: createHistory(process.env.VUE_ROUTER_BASE),
  });

  // Перенаправление на страницу входа
  Router.beforeEach(async (to, from): Promise<any> => {
    // Это инициализируется после App.vue
    const store = useCurrentUserStore();
    const networkstore = useNetworkStore();

    // если в LocalStorage есть ключ redirect И нет ключа isAuthenticated и нет authenticated в query
    if (
      LocalStorage.getItem('redirect') &&
      !LocalStorage.getItem('isAuthenticated') &&
      !('authenticated' in to.query)
    ) {
      // если в localStorage есть ключ redirect, то получаем его значения
      const { path, query, logout } = LocalStorage.getItem('redirect') as {
        path: string;
        query: string;
        logout: boolean;
      };
      // если logout === true, то значит мы только что пришли после логаута
      if (logout === true) {
        LocalStorage.remove('redirect');
        // значит перенаправляем роутер по маршруту из поля redirect
        return { path, replace: true, query };
      }
    }

    //если в localStorage есть флаг isAuthenticated а пользователь не получен, то нужно попытаться его получить
    if (
      LocalStorage.getItem('isAuthenticated') &&
      store.currentUser === null &&
      networkstore.isOnline
    ) {
      try {
        // вызываем действие onGetUser хранилища для получения данных пользователя
        const data = await store.onGetUser();
        debugLog('responseUserRefresh:', data);
        //если пользователь получен, то отправляем роутер по маршруту который запрашивался
        return { path: to.path, replace: true, query: to.query };
      } catch (e) {
        debugLog('errorUserRefresh:', e);
        //если пользователь не получен, то отправляем роутер на страницу логаута, если ошибка была ERR_NO_USER, то вызываем действие onLogout хранилища, которое удаляет isAuthenticated из LocalStorage и обнуляет текущего пользователя
        if (e.data?.error?.code === 'ERR_NO_USER') {
          store.onLogout();
        }

        return { name: 'logout', query: to.query };
      }
    }
    //если в query есть поле authenticated, то нужно попытаться получить пользователя
    if ('authenticated' in to.query && networkstore.isOnline) {
      try {
        const data = await store.onGetUser();
        debugLog('responseUserFromRouter:', data);
        //если пользователь получен, то зачищаем query
        const { authenticated, ...rest } = to.query;
        if (LocalStorage.getItem('redirect')) {
          const { path, query } = LocalStorage.getItem('redirect') as {
            path: string;
            query: string;
          };
          LocalStorage.remove('redirect');
          return { path: path, replace: true, query: query };
        }

        return { path: to.path, replace: true, query: rest };
      } catch (e) {
        debugLog('errorUserFromRouter:', e);
        //если пользователь не получен, то зачищаем query и зачищаем остатки авторизации

        const { authenticated, ...rest } = to.query;
        if (LocalStorage.getItem('redirect')) {
          LocalStorage.remove('redirect');
        }

        if (e.data?.error?.code === 'ERR_NO_USER') {
          store.onLogout();
        }

        return { name: 'logout', query: rest };
      }
    }
    return;

    // if (to.query?.redirect) {
    //   const { redirect, ...rest } = to.query;

    //   return { path: redirect, query: rest };
    // } else return true;
  });

  return Router;
});

/*

import { debugLog } from '../utils/';
import { route } from 'quasar/wrappers';
import {
  createRouter,
  createMemoryHistory,
  createWebHistory,
  createWebHashHistory,
} from 'vue-router';
import routes from './routes';
import { LocalStorage } from 'quasar';
import { useCurrentUserStore } from 'src/stores/current-user';

// Логгирование пути текущего файла
debugLog('/src/router/index.js');

export default route(function () {
  // Определение метода истории в зависимости от настроек окружения
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : (process.env.VUE_ROUTER_MODE === 'history' ? createWebHistory : createWebHashHistory)();

  // Создание экземпляра маршрутизатора
  const Router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,
    history: createHistory,
  });

  // Функция для аутентификации пользователя
  async function authenticateUser(store) {
    try {
      const data = await store.onGetUser();
      debugLog('responseUserRefresh:', data);
      return true;
    } catch (e) {
      debugLog('errorUserRefresh:', e);

      if (e.data?.error?.code === 'ERR_NO_USER') {
        store.onLogout();
      }
      return false;
    }
  }

  // Функция для очистки query-параметров
  function cleanQuery(query) {
    const { authenticated, ...rest } = query;
    return rest;
  }

  // Глобальная проверка перед каждым переходом
  Router.beforeEach(async (to, from) => {
    const store = useCurrentUserStore();

    // Проверка и обработка перенаправлений из LocalStorage
    if (LocalStorage.getItem('redirect')) {
      const redirect = LocalStorage.getItem('redirect');
      LocalStorage.remove('redirect');
      return { path: redirect.path, replace: true, query: redirect.query };
    }

    // Проверка аутентификации и обновление информации о пользователе
    if (LocalStorage.getItem('isAuthenticated') && store.currentUser === null) {
      const isAuthenticated = await authenticateUser(store);
      if (isAuthenticated) {
        return { path: to.path, replace: true, query: to.query };
      } else {
        return { name: 'logout', query: cleanQuery(to.query) };
      }
    }

    // Обработка query-параметра 'authenticated'
    if ('authenticated' in to.query) {
      const isAuthenticated = await authenticateUser(store);
      if (isAuthenticated) {
        return { path: to.path, replace: true, query: cleanQuery(to.query) };
      } else {
        return { name: 'logout', query: cleanQuery(to.query) };
      }
    }

    return true;
  });

  return Router;
});


*/
