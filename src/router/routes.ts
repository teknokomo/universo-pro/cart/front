import { LocalStorage } from 'quasar';
import { debugLog } from '../utils';
import { RouteRecordRaw } from 'vue-router';

debugLog('/src/router/routes.js');

function apiUriString({ to, operation, from }): string {
  const queryString =
    '?' +
    Object.keys(from.query)
      .map((key) => {
        return from.query[key] !== null
          ? `${key}=${encodeURIComponent(from.query[key])}`
          : `${key}`;
      })
      .join('&');
  let apiUrl = process.env.PROD
    ? 'https://cart.universo.pro/universo/oauth/v2/' + operation
    : 'http://127.0.0.1:3000/universo/oauth/v2/' + operation;
  apiUrl =
    queryString.length > 1
      ? apiUrl + queryString + '&redirect=' + from.path
      : apiUrl + '?redirect=' + from.path;

  return apiUrl;
}

// Описание маршрутизации
// https://vueschool.io/courses/vue-router-4-for-everyone

// Перенаправление
// this.$router.push(redirectPath);

// Получение значения параметра query authenticated
// this.$router.query.authenticated

// Параметры маршрута
// /carts/:id - id это параметр
const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('src/layouts/MainLayout.vue'),
    redirect: '/carts',
    children: [
      {
        path: 'login',
        name: 'Вход',
        component: () => import('src/pages/Login.vue'),
      },
      {
        path: 'logout',
        name: 'Выход',
        component: () => import('src/pages/Logout.vue'),
      },
      {
        path: 'carts',
        name: 'Списки',
        component: () => import('src/pages/PublicCarts.vue'),
      },
      {
        path: 'carts/:id',
        name: 'Список',
        component: () => import('src/pages/PublicCart.vue'),
      },
      {
        path: 'carts/:id/users',
        name: 'Пользователи',
        component: () => import('src/pages/PublicCartUsers.vue'),
      },
      {
        path: 'help',
        name: 'Помощь',
        component: () => import('src/pages/Help.vue'),
      },
      {
        path: 'profile',
        name: 'Профиль',
        component: () => import('src/pages/Profile.vue'),
        meta: {
          requiresAuth: true,
        },
      },
      {
        path: 'universo/oauth/v2/login',
        name: 'login',
        component: () => {},
        beforeEnter: (to, from) => {
          LocalStorage.set('redirect', {
            path: from.path,
            query: from.query,
          });

          window.location.href = apiUriString({
            to,
            from,
            operation: 'login',
          });
          // return "/"; // not important since redirecting
        },
      },
      {
        path: 'universo/oauth/v2/logout',
        name: 'logout',
        component: () => {},
        beforeEnter: (to, from) => {
          LocalStorage.set('redirect', {
            path: from.path,
            query: from.query,
            logout: true,
          });
          window.location.href = apiUriString({
            to,
            from,
            operation: 'logout',
          });
          // return "/"; // not important since redirecting
        },
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('../pages/ErrorNotFound.vue'),
    // redirect: "/carts",
  },
];

export default routes;
