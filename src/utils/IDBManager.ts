export interface ProviderId {
  provider_id?: number;
  provider_url: string;
}

export interface Item {
  id?: number;
  name: string;
  quantity_plan: number;
  quantity_fact: number;
  picked: boolean;
}

export interface Profile {
  last_name: string;
  first_name: string;
  sex: string;
}

export interface User {
  id: string;
  profile: Profile;
  is_owner: boolean;
}

export interface ProviderCart {
  id: number;
  is_synced: boolean;
  is_deleted: boolean;
}

export interface Provider {
  id: number;
  cart: ProviderCart;
}

export interface Cart {
  replica_id?: number;
  id?: number;
  local_id?: number;
  provider: Provider;
  name: string;
  users?: User[];
  items?: Item[];
}

export enum StorageEvent {
  AddCart = 'addcart',
  ChangeCart = 'changecart',
  DeleteCart = 'deletecart',

  // ... другие события при необходимости ...
}

// Определение класса для работы с IndexedDB
export class IndexedDBStorage {
  // Свойство для хранения экземпляра базы данных
  private db: IDBDatabase | null = null;

  // Объект для хранения подписчиков для каждого типа событий
  private eventSubscribers = new Map<StorageEvent, Set<Function>>();

  /**
   * Creates an instance of IndexedDBStorage.
   * Конструктор класса, принимающий название базы данных
   * @param {string} dbName
   * @memberof IndexedDBStorage
   */
  constructor(private dbName: string) {
    // Инициализация базы данных при создании экземпляра класса
    this.checkPrefixes();
    this.init();
  }

  /**
   *  Метод для подписки на события
   *
   * @param {StorageEvent} eventType
   * @param {Function} callback
   * @memberof IndexedDBStorage
   */
  public on(eventType: StorageEvent, callback: Function) {
    if (!this.eventSubscribers.has(eventType)) {
      this.eventSubscribers.set(eventType, new Set<Function>());
    }
    this.eventSubscribers.get(eventType)!.add(callback);
  }

  /**
   * Вспомогательный метод для триггера событий
   *
   * @private
   * @param {StorageEvent} eventType
   * @param {...any[]} args
   * @memberof IndexedDBStorage
   */
  private triggerEvent(eventType: StorageEvent, ...args: any[]) {
    const subscribers = this.eventSubscribers.get(eventType);
    if (subscribers) {
      for (const callback of subscribers) {
        callback(...args);
      }
    }
  }

  /**
   * Асинхронный метод для инициализации базы данных
   *
   * @private
   * @memberof IndexedDBStorage
   */
  private async init() {
    try {
      // Открытие или создание базы данных
      const request = indexedDB.open(this.dbName, 1);

      // Обработка события необходимости обновления базы данных
      request.onupgradeneeded = this.handleUpgradeNeeded.bind(this);

      // Ожидание успешного выполнения запроса и сохранение экземпляра базы данных
      this.db = await this.promisifyRequest(request);
    } catch (error) {
      // Обработка ошибок при инициализации базы данных
      console.error('Database initialization failed:', error);
    }
  }

  /**
   * Метод для обработки события обновления базы данных
   *
   * @private
   * @param {IDBVersionChangeEvent} event
   * @memberof IndexedDBStorage
   */
  private handleUpgradeNeeded(event: IDBVersionChangeEvent) {
    // Получение экземпляра базы данных из события
    this.db = (event.target as IDBOpenDBRequest).result;

    // Создание хранилищ, если они еще не существуют
    this.createStore('carts', 'local_id', true);
    this.createStore('providers', 'provider_id', true);
  }

  /**
   * Метод для создания хранилища в базе данных
   *
   * @private
   * @param {string} storeName
   * @param {string} keyPath
   * @param {boolean} [autoIncrement=false]
   * @memberof IndexedDBStorage
   */
  private createStore(
    storeName: string,
    keyPath: string,
    autoIncrement = false,
  ) {
    // Проверка, существует ли уже хранилище, и его создание в случае отсутствия
    if (!this.db!.objectStoreNames.contains(storeName)) {
      this.db!.createObjectStore(storeName, {
        keyPath,
        autoIncrement,
      });
    }
  }

  /**
   * Метод для получения объекта хранилища из базы данных
   *
   * @private
   * @param {string} storeName
   * @param {IDBTransactionMode} mode
   * @return {*}
   * @memberof IndexedDBStorage
   */
  private getObjectStore(storeName: string, mode: IDBTransactionMode) {
    // Создание транзакции и получение хранилища
    return this.db!.transaction(storeName, mode).objectStore(storeName);
  }

  /**
   * Обобщенный метод для операций с объектами в хранилище
   *
   * @private
   * @template T
   * @param {string} storeName
   * @param {IDBTransactionMode} mode
   * @param {((store: IDBObjectStore) => IDBRequest<T> | Promise<T>)} callback
   * @return {*}  {Promise<T>}
   * @memberof IndexedDBStorage
   */
  private async operateOnStore<T>(
    storeName: string,
    mode: IDBTransactionMode,
    callback: (store: IDBObjectStore) => IDBRequest<T> | Promise<T>,
  ): Promise<T> {
    const store = this.getObjectStore(storeName, mode);
    const result = callback(store);
    return result instanceof Promise ? result : this.promisifyRequest(result);
  }
  /**
   * Метод для установки флага is_synced на корзине
   *
   * @param {number} cartId
   * @param {boolean} isSynced
   */
  public async setCartSynced(cartId: number, isSynced: boolean) {
    const cart = await this.getCart(cartId);
    if (!cart) {
      throw new Error(`Cart with local_id ${cartId} not found`);
    }

    cart.provider.cart.is_synced = isSynced;
    await this.saveCart(cart);
  }

  /**
   * Метод для установки флага is_deleted на корзине
   *
   * @param {number} cartId
   * @param {boolean} isDeleted
   */
  public async setCartDeleted(cartId: number, isDeleted: boolean) {
    const cart = await this.getCart(cartId);
    if (!cart) {
      throw new Error(`Cart with local_id ${cartId} not found`);
    }

    cart.provider.cart.is_deleted = isDeleted;
    await this.saveCart(cart);
  }

  /**
   * Метод для сохранения объекта в хранилище carts
   *
   * @param {Cart} cart
   * @memberof IndexedDBStorage
   */
  public async saveCart(cart: Cart) {
    let isNewCart = false;

    // Проверяем, является ли это новой корзиной
    if (!cart.local_id) {
      cart.local_id = await this.getNextLocalId();
      isNewCart = true;
    }

    const existingCart = cart.local_id
      ? await this.getCart(cart.local_id)
      : null;

    // Сохраняем или обновляем только если корзины отличаются или это новая корзина
    if (
      isNewCart ||
      !existingCart ||
      this.cartsAreDifferent(cart, existingCart)
    ) {
      await this.operateOnStore('carts', 'readwrite', (store) =>
        store.put(cart),
      );
      this.triggerEvent(
        isNewCart ? StorageEvent.AddCart : StorageEvent.ChangeCart,
        cart,
      );
      return cart;
    }
    return cart;
  }

  /**
   * Метод возвращает следующий local_id
   *
   * @return {*}  {Promise<number>}
   * @memberof IndexedDBStorage
   */
  public async getNextLocalId(): Promise<number> {
    const store = this.getObjectStore('carts', 'readonly');
    const request = store.getAll();

    // Используем промис для асинхронной обработки запроса
    return new Promise((resolve, reject) => {
      request.onsuccess = () => {
        const carts = request.result as Cart[];
        const sortedIds = carts
          .map((cart) => cart.local_id)
          .sort((a, b) => a - b);

        let nextId = 1;
        for (const id of sortedIds) {
          if (id !== nextId) {
            resolve(nextId); // Найдена "дыра" в идентификаторах
            return;
          }
          nextId++;
        }

        resolve(nextId); // Возвращаем следующий идентификатор после последнего
      };

      request.onerror = () => reject(request.error);
    });
  }
  /**
   * Метод для получения объекта из хранилища carts
   *
   * @param {number} cartId
   * @return {*}
   * @memberof IndexedDBStorage
   */
  public async getCart(cartId: number) {
    return await this.operateOnStore('carts', 'readonly', (store) =>
      store.get(cartId),
    );
  }
  /**
   * Метод для удаления корзины по local_id
   *
   * @param {string} cartId
   * @memberof IndexedDBStorage
   */
  public async deleteCart(cartId: number) {
    const cart = await this.operateOnStore('carts', 'readonly', (store) =>
      store.get(cartId),
    );
    if (cart?.local_id) {
      await this.operateOnStore('carts', 'readwrite', (store) =>
        store.delete(cart.local_id),
      );

      this.triggerEvent(StorageEvent.DeleteCart, cart); // Триггер события удаления корзины
    } else {
      throw new Error(`Cart with local_id ${cartId} not found`);
    }
  }
  /**
   * Метод для удаления элемента из корзины.
   *
   * @param {number} cartId - Идентификатор корзины.
   * @param {number} itemId - Идентификатор элемента для удаления.
   * @memberof IndexedDBStorage
   */
  public async removeItemFromCart(cartId: number, itemId: number) {
    // Получаем корзину по идентификатору.
    const cart = await this.getCart(cartId);
    if (!cart) {
      throw new Error(`Cart with local_id ${cartId} not found`);
    }

    // Проверяем наличие элементов в корзине.
    if (!cart.items || cart.items.length === 0) {
      throw new Error(`No items found in cart with local_id ${cartId}`);
    }

    // Удаляем элемент из массива items.
    cart.items = cart.items.filter((item) => item.id !== itemId);

    // Сохраняем обновленную корзину.
    await this.saveCart(cart);
  }
  /**
   * Глубокое сравнение двух объектов Cart.
   *
   * @param {Cart} newCart
   * @param {Cart} existingCart
   * @return {boolean} Возвращает true, если объекты различаются.
   */
  private cartsAreDifferent(newCart: Cart, existingCart: Cart): boolean {
    //удаляем из копии newCart и existingCart поля id и local_id если они есть
    newCart = { ...newCart };
    delete newCart.local_id;
    delete newCart.id;
    delete newCart.replica_id;
    existingCart = { ...existingCart };
    delete existingCart.local_id;
    delete existingCart.id;
    delete existingCart.replica_id;

    // Сравниваем простые поля
    for (const key in newCart) {
      if (
        typeof newCart[key] !== 'object' ||
        typeof existingCart[key] !== 'object'
      ) {
        if (newCart[key] !== existingCart[key]) {
          return true;
        }
      }
    }
    //сравниваем объект provider
    if (newCart.provider && existingCart.provider) {
      if (newCart.provider.id !== existingCart.provider.id) return true;
      if (newCart.provider.cart.id !== existingCart.provider.cart.id)
        return true;
      if (
        newCart.provider.cart.is_deleted !==
        existingCart.provider.cart.is_deleted
      )
        return true;
      if (
        newCart.provider.cart.is_synced !== existingCart.provider.cart.is_synced
      )
        return true;
    }

    // Сравниваем массивы items
    if (newCart.items && existingCart.items) {
      if (newCart.items.length !== existingCart.items.length) return true;
      for (let i = 0; i < newCart.items.length; i++) {
        if (this.itemsAreDifferent(newCart.items[i], existingCart.items[i]))
          return true;
      }
    }
    // Сравниваем массивы users
    if (newCart.users && existingCart.users) {
      if (this.usersAreDifferent(newCart.users, existingCart.users))
        return true;
    } else if (newCart.users || existingCart.users) {
      // Если один из массивов users отсутствует
      return true;
    }
    return false;
  }
  /**
   * Глубокое сравнение двух массивов пользователей.
   *
   * @param {User[]} newCartUsers
   * @param {User[]} existingCartUsers
   * @return {boolean} Возвращает true, если массивы пользователей различаются.
   */
  private usersAreDifferent(
    newCartUsers: User[],
    existingCartUsers: User[],
  ): boolean {
    if (newCartUsers.length !== existingCartUsers.length) return true;
    for (let i = 0; i < newCartUsers.length; i++) {
      const newUser = newCartUsers[i];
      const existingUser = existingCartUsers[i];

      // Сравниваем поля объекта User и связанный объект Profile
      if (
        newUser.id !== existingUser.id ||
        newUser.is_owner !== existingUser.is_owner ||
        newUser.profile.first_name !== existingUser.profile.first_name ||
        newUser.profile.last_name !== existingUser.profile.last_name ||
        newUser.profile.sex !== existingUser.profile.sex
      ) {
        return true;
      }
    }
    return false;
  }

  /**
   * Глубокое сравнение двух объектов Item.
   *
   * @param {Item} item1
   * @param {Item} item2
   * @return {boolean} Возвращает true, если объекты различаются.
   */
  private itemsAreDifferent(item1: Item, item2: Item): boolean {
    // Сравниваем поля объекта Item
    return Object.keys(item1).some((key) => item1[key] !== item2[key]);
  }

  /**
   * Метод для сохранения объекта в хранилище providers
   *
   * @param {ProviderId} provider
   * @memberof IndexedDBStorage
   */
  public async saveProvider(provider: ProviderId) {
    await this.operateOnStore('providers', 'readwrite', (store) =>
      store.put(provider),
    );
  }
  /**
   * Метод для сохранения объекта в хранилище providers по url
   *
   * @param {ProviderId} provider
   * @memberof IndexedDBStorage
   */
  public async saveProviderByUrl(provider: ProviderId) {
    const providerByUrl = await this.getAllProviders().then((providers) =>
      providers.find((p) => p.provider_url === provider.provider_url),
    );
    await this.operateOnStore('providers', 'readwrite', (store) =>
      store.put(providerByUrl ? providerByUrl : provider),
    );
  }

  /**
   * Метод для получения объекта из хранилища providers
   *
   * @param {number} providerId
   * @return {*}
   * @memberof IndexedDBStorage
   */
  public async getProvider(providerId: number) {
    return await this.operateOnStore('providers', 'readonly', (store) =>
      store.get(providerId),
    );
  }
  /**
   * Асинхронный метод для получения всех корзин из хранилища 'carts'.
   * @return {Promise<Cart[]>} Возвращает промис с массивом всех корзин.
   */
  public async getAllCarts(): Promise<Cart[]> {
    return this.operateOnStore('carts', 'readonly', (store) => {
      const request = store.getAll();
      return this.promisifyRequest<Cart[]>(request);
    });
  }

  /**
   * Асинхронный метод для получения всех провайдеров из хранилища 'providers'.
   * @return {Promise<ProviderId[]>} Возвращает промис с массивом всех провайдеров.
   */
  public async getAllProviders(): Promise<ProviderId[]> {
    return this.operateOnStore('providers', 'readonly', (store) => {
      const request = store.getAll();
      return this.promisifyRequest<ProviderId[]>(request);
    });
  }
  /**
   * Получение всех корзин, связанных с определенным providerId
   *
   * @param {number} providerId
   * @return {*}
   * @memberof IndexedDBStorage
   */
  public async getCartsByProvider(providerId: number) {
    const store = this.getObjectStore('carts', 'readonly');
    const request = store.openCursor();

    const carts: Cart[] = [];
    await new Promise<void>((resolve, reject) => {
      request.onsuccess = (event) => {
        const cursor = (event.target as IDBRequest<IDBCursorWithValue>).result;
        if (cursor) {
          const cart: Cart = cursor.value;
          if (cart.provider.id === providerId) {
            carts.push(cart);
          }
          cursor.continue();
          return;
        }
        resolve();
      };
      request.onerror = () => reject(request.error);
    });

    return carts;
  }
  /**
   * Метод для обработки массива корзин. Проверяет каждую корзину на наличие в хранилище и
   * либо обновляет существующую, либо добавляет новую.
   *
   * @param {Cart[]} carts - Массив корзин для обработки.
   * @memberof IndexedDBStorage
   */
  async processCarts(carts) {
    for (const cart of carts) {
      try {
        await this.saveOrUpdateCartByProvider(cart);
      } catch (error) {
        console.error(`Ошибка при обработке корзины: ${error.message}`);
      }
    }
  }
  /**
   * Метод для добавления или обновления корзины
   *
   * @param {Cart} cart
   * @memberof IndexedDBStorage
   */
  public async upsertCart(cart: Cart) {
    if (!cart.provider.cart.id) {
      throw new Error('Provider cart ID is required for upserting a cart');
    }
    const existingCart = await this.getCartByProviderCartId(
      cart.provider.cart.id.toString(),
    );
    if (existingCart) {
      cart.local_id = existingCart.local_id; // Переназначаем local_id существующей корзины
    }
    await this.saveCart(cart);
  }

  /**
   * Метод для получения корзины по provider_cart_id
   *
   * @param {string} providerCartId
   * @return {*} {(Promise<Cart>)}
   * @memberof IndexedDBStorage
   */
  public async getCartByProviderCartId(providerCartId: string) {
    const store = this.getObjectStore('carts', 'readonly');
    const request = store.openCursor();

    return new Promise<Cart | undefined>((resolve, reject) => {
      request.onsuccess = (event) => {
        const cursor = (event.target as IDBRequest<IDBCursorWithValue>).result;
        if (cursor) {
          const cart: Cart = cursor.value;
          if (cart.provider.cart.id.toString() === providerCartId) {
            resolve(cart);
            return;
          }
          cursor.continue();
          return;
        }
        resolve(undefined);
      };
      request.onerror = () => reject(request.error);
    });
  }

  /**
   * Метод для сохранения или обновления корзины на основе provider_id и provider_cart_id
   *
   * @param {Cart} cart
   * @memberof IndexedDBStorage
   */
  public async saveOrUpdateCartByProvider(cart: Cart) {
    if (!cart.provider.id || !cart.provider.cart.id) {
      throw new Error('Provider ID and Provider Cart ID are required');
    }

    // Поиск существующей корзины с такими же provider_id и provider_cart_id
    const existingCart = await this.findCartByProviderIds(
      cart.provider.id,
      cart.provider.cart.id,
    );

    if (existingCart) {
      // Если корзина найдена, обновляем её
      const updatedCart = { ...existingCart, ...cart };
      await this.saveCart(updatedCart);
    } else {
      // Если нет, сохраняем как новую корзину
      await this.saveCart(cart);
    }
  }

  /**
   * Вспомогательный метод для поиска корзины по provider_id и provider_cart_id
   *
   * @private
   * @param {number} providerId
   * @param {number} providerCartId
   * @return {*}  {(Promise<Cart | null>)}
   * @memberof IndexedDBStorage
   */
  private async findCartByProviderIds(
    providerId: number,
    providerCartId: number,
  ): Promise<Cart | null> {
    const store = this.getObjectStore('carts', 'readonly');
    const request = store.openCursor();

    return new Promise((resolve, reject) => {
      request.onsuccess = (event) => {
        const cursor = (event.target as IDBRequest<IDBCursorWithValue>).result;
        if (cursor) {
          const cart: Cart = cursor.value;
          if (
            cart.provider.id === providerId &&
            cart.provider.cart.id === providerCartId
          ) {
            resolve(cart);
            return;
          }
          cursor.continue();
          return;
        }
        resolve(null);
      };
      request.onerror = () => reject(request.error);
    });
  }

  /**
   * Метод для удаления корзины по provider_cart_id
   *
   * @param {string} providerCartId
   * @memberof IndexedDBStorage
   */
  public async deleteCartByProviderCartId(providerCartId: string) {
    const cart = await this.getCartByProviderCartId(providerCartId);
    if (cart?.local_id) {
      await this.operateOnStore('carts', 'readwrite', (store) =>
        store.delete(cart.local_id),
      );
      this.triggerEvent(StorageEvent.DeleteCart, cart); // Триггер события удаления корзины
    } else {
      throw new Error(`Cart with provider_cart_id ${providerCartId} not found`);
    }
  }

  /**
   * Метод для получения одного элемента из корзины
   *
   * @param {number} cartId - Идентификатор корзины
   * @param {number} itemId - Идентификатор элемента
   * @return {*}  {(Promise<Item | undefined>)} - Возвращает найденный элемент или undefined
   * @memberof IndexedDBStorage
   */
  public async getItemFromCart(
    cartId: number,
    itemId: number,
  ): Promise<Item | undefined> {
    // Получение корзины
    const cart = await this.getCart(cartId);
    if (!cart) {
      throw new Error(`Cart with local_id ${cartId} not found`);
    }

    // Поиск элемента в корзине
    const item = cart.items?.find((item) => item.id === itemId);
    if (!item) {
      throw new Error(`Item with id ${itemId} not found in cart ${cartId}`);
    }

    return item;
  }
  /**
   * Метод для обновления элемента в корзине
   *
   * @param {number} cartId
   * @param {number} itemId
   * @param {Partial<Item>} updatedFields
   * @memberof IndexedDBStorage
   */
  public async updateItemInCart(
    cartId: number,
    itemId: number,
    updatedFields: Partial<Item>,
  ) {
    const cart = await this.getCart(cartId);
    if (!cart) {
      throw new Error(`Cart with local_id ${cartId} not found`);
    }
    if (!cart.items) {
      throw new Error(`No items found in cart with local_id ${cartId}`);
    }

    const itemIndex = cart.items.findIndex((item) => item.id === itemId);
    if (itemIndex === -1) {
      throw new Error(`Item with id ${itemId} not found in cart`);
    }

    // Обновление полей элемента
    const itemToUpdate = cart.items[itemIndex];
    cart.items[itemIndex] = { ...itemToUpdate, ...updatedFields };

    // Сохранение изменений в корзине
    await this.saveCart(cart);
  }

  /**
   * Метод для получения provider_url корзины по local_id
   *
   * @param {number} cartId
   * @return {*}  {Promise<string>}
   * @memberof IndexedDBStorage
   */
  public async getProviderUrlByCartId(cartId: number): Promise<string> {
    // Получение корзины
    const cart = await this.getCart(cartId);
    if (!cart) {
      throw new Error(`Cart with local_id ${cartId} not found`);
    }

    // Проверка наличия provider_id в корзине
    if (!cart.provider_id) {
      throw new Error(
        `Cart with local_id ${cartId} does not have a provider_id`,
      );
    }

    // Получение провайдера по provider_id
    const provider = await this.getProvider(cart.provider_id);
    if (!provider) {
      throw new Error(
        `Provider with provider_id ${cart.provider_id} not found`,
      );
    }

    return provider.provider_url; // Возвращаем URL провайдера
  }

  /**
   * Вспомогательная функция для преобразования IDBRequest в Promise
   *
   * @private
   * @template T
   * @param {IDBRequest<T>} request
   * @return {*}  {Promise<T>}
   * @memberof IndexedDBStorage
   */
  private promisifyRequest<T = undefined>(request: IDBRequest<T>): Promise<T> {
    return new Promise((resolve, reject) => {
      request.onsuccess = () => resolve(request.result);
      request.onerror = () => reject(request.error);
    });
  }

  /**
   * Проверка поддержки IndexedDB
   *
   * @private
   * @memberof IndexedDBStorage
   */
  private checkPrefixes() {
    // проверяем существования префикса.
    const indexedDB =
      window.indexedDB || //@ts-ignore
      window.mozIndexedDB || //@ts-ignore
      window.webkitIndexedDB || //@ts-ignore
      window.msIndexedDB;
    // также могут отличаться и window.IDB* objects: Transaction, KeyRange и тд
    const IDBTransaction =
      window.IDBTransaction || //@ts-ignore
      window.webkitIDBTransaction || //@ts-ignore
      window.msIDBTransaction;
    const IDBKeyRange = //@ts-ignore
      window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
    // (Mozilla никогда не создавала префиксов для объектов, поэтому window.mozIDB* не требуется проверять)

    if (indexedDB) {
      console.log('IndexedDB поддерживается');
    } else {
      console.log('Ваш браузер не поддерживает IndexedDB');
    }
  }
}
