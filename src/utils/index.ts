import { LocalStorage } from 'quasar';

/**
 * Выводит лог в зависимости от установки переменной окружения DEBUG
 *
 * @export
 * @param {string} arguments набор данных для вывода в лог
 */

const STYLE = `
  color: #FFFFFF;
  background: #232323;
  text-shadow: 0 0 5px #FFF, 0 0 10px #FFF, 0 0 15px #FFF, 0 0 20px #49ff18, 0 0 30px #49FF18, 0 0 40px #49FF18, 0 0 55px #49FF18, 0 0 75px #49ff18;
  width:200px;
`;
const LOG_PREFIX = '%cDEBUG >>>';
const SUBSTRING = './src/';

function isDebugActive(urlSearchParams, localStorageItem) {
  return (
    urlSearchParams.has('debug') ||
    localStorageItem === true ||
    process.env.DEBUG === 'true'
  );
}

function extractRelevantStackTrace() {
  const err = new Error();
  return err.stack
    .split('\n')
    .filter((item) => item.includes(SUBSTRING))
    .map((item) => item.slice(item.lastIndexOf(SUBSTRING) + SUBSTRING.length))
    .filter((item) => !item.includes('utils/index.js'))
    .join('\n');
}

export function debugLog(...args) {
  if (
    isDebugActive(
      new URLSearchParams(window.location.search),
      LocalStorage.getItem('debug'),
    )
  ) {
    console.log(`${LOG_PREFIX} `, STYLE, ...args);
    console.groupCollapsed('Стек вызовов:');
    console.log(extractRelevantStackTrace());
    console.groupEnd();
  }
}

/**
 *
 *
 * @export
 * @param {*} item
 */
export function test(item) {
  console.log(item);

  const a = [
    { name: 'Project 0', id: 0 },
    { name: 'Project 1', id: 1 },
    { name: 'Project 2', id: 2 },
    { name: 'Project 3', id: 3 },
    { name: 'Project 4', id: 4 },
    { name: 'Project 5', id: 5 },
    { name: 'Project 6', id: 6 },
    { name: 'Project 7', id: 7 },
    { name: 'Project 8', id: 8 },
    { name: 'Project 9', id: 9 },
  ];
}
