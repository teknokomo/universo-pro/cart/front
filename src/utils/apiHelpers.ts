import { AxiosResponse } from 'axios';

// Функция для обработки успешного ответа от API
export function handleApiResponse<T>(
  response: AxiosResponse<T>,
  onSuccess: (data: T) => any,
): any {
  if (response.status === 200) {
    onSuccess(response.data);
  } else {
    console.error('Ошибка API:', response.statusText);
    throw new Error(response.statusText);
  }
}

// Функция для обработки ошибок API
export function handleApiError(error: unknown): void {
  if (error instanceof Error) {
    console.error('Ошибка API:', error.message);
    throw error; // или можно обработать ошибку специфичным образом
  } else {
    console.error('Неизвестная ошибка:', error);
    throw new Error('Неизвестная ошибка');
  }
}
