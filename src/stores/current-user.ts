import { debugLog } from '../utils';

debugLog('/src/stores/current-user.js');

import { defineStore, acceptHMRUpdate } from 'pinia';

import { backend } from 'src/boot/axios';

import { Cookies, LocalStorage } from 'quasar';
import { useNetworkStore } from './network';

export const useCurrentUserStore = defineStore('currentUser', {
  state: () => ({
    currentUser: null,
    defaultImg: {
      male: 'images/male.svg',
      female: 'images/female.svg',
      ufo: 'images/ufo.svg',
    },
  }),
  getters: {
    getCurrentUser: (state) => state.currentUser,
    getAvatar(state) {
      return state.currentUser?.avatar?.bildoF?.url || this.getFallbackAvatar;
    },
    getFallbackAvatar: (state) => {
      return state.defaultImg[state.currentUser?.sex || 'ufo'];
    },
    getCover: (state) => {
      return (
        state.currentUser?.cover?.bildoF?.url ||
        'https://cdn.quasar.dev/img/material.png'
      );
    },
    isLoggedIn: (state) => {
      return !!state.currentUser?.id;
    },
  },
  actions: {
    onLogout() {
      this.currentUser = null;
      LocalStorage.remove('isAuthenticated');
      Cookies.remove('sessionid');
      Cookies.remove('card-sid');
      Cookies.remove('isLoggedin');
      Cookies.remove('csrftoken');
    },

    async onGetUser() {
      const networkStore = useNetworkStore();
      if (!networkStore.isOnline) return Promise.reject();
      try {
        const resp = await backend('/user');
        if (resp.status == 200) {
          if (!resp.data?.error) {
            this.currentUser = resp.data;
            LocalStorage.set('isAuthenticated', true);

            return Promise.resolve(resp.data);
          }
        }
        this.onLogout();
        return Promise.reject(resp);
      } catch (e) {
        this.onLogout();
        console.log('Неизвестная ошибка!!!');
        return Promise.reject(e);
      }
    },
  },
});
//@ts-ignore
if (import.meta.hot) {
  //@ts-ignore
  import.meta.hot.accept(acceptHMRUpdate(useCurrentUserStore, import.meta.hot));
}
