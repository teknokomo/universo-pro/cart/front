// store/network.js
import { defineStore } from 'pinia';

export const useNetworkStore = defineStore('network', {
  state: () => ({
    isOnline: navigator.onLine,
    isSyncing: false,
  }),
  actions: {
    setOnlineStatus(status) {
      this.isOnline = status;
    },

    async syncDataWithBackend() {
      if (!this.isOnline) return;

      this.setSyncingStatus(true);

      // try {
      //   const items = await db.items.toArray();
      //   // Отправить данные на бэкенд
      //   // ...

      //   // Очистить IndexedDB после успешной синхронизации
      //   // await db.items.clear();
      // } catch (error) {
      //   console.error('Ошибка синхронизации:', error);
      // } finally {
      //   this.setSyncingStatus(false);
      // }
    },
  },
});
