import { defineStore } from 'pinia';
import { useNetworkStore } from './network';
import { idb } from '../boot/indexedDB';
import { backend } from 'src/boot/axios';
import { handleApiResponse, handleApiError } from '../utils/apiHelpers';
import { Cart, Item, StorageEvent } from 'src/utils/IDBManager';

export const useAllCartsStore = defineStore('allCarts', {
  // Определение состояния хранилища
  state: (): { carts: Cart[] } => ({
    carts: [], // Инициализация корзин как пустого массива
  }),
  getters: {
    // Геттер для получения списка корзин
    getCarts: (state): Cart[] =>
      state.carts.filter((cart) => !cart.provider.cart.is_deleted),

    isSynced: (state): boolean =>
      !state.carts.some((cart) => !cart.provider.cart.is_synced),

    // Геттер для получения копии корзины с сортированными товарами
    getCart: (state) => {
      return (cartId: number): Cart | null => {
        const cart = state.carts.find((cart) => cart.local_id === cartId);
        if (!cart || cart.provider.cart.is_deleted) return null; // Возвращение null, если корзина пуста

        const copyCart = { ...cart }; // Создание копии текущей корзины
        if (copyCart.items && copyCart.items.length > 0) {
          // Сортировка товаров в корзине по убыванию ID, если они есть
          copyCart.items = copyCart.items.slice().sort((a, b) => b.id - a.id);
        }
        return copyCart;
      };
    },

    // Геттер для получения названия маршрута текущей корзины
    getRouteName: (state) => {
      return (cartId: number): string => {
        const cart = state.carts.find((cart) => cart.local_id === cartId);
        return cart ? cart.name : ''; // Возвращение названия или пустой строки
      };
    },
  },
  actions: {
    // Проверка наличия интернет соединения
    async checkOnlineStatus(): Promise<void> {
      const networkStore = useNetworkStore();
      if (!networkStore.isOnline) {
        throw new Error('Нет подключения к интернету');
      }
    },
    async updateCartFromIDB() {
      await idb.getAllCarts().then((carts) => {
        this.carts = carts;
      });
    },

    async initCarts() {
      await this.updateCartFromIDB();
      idb.on(StorageEvent.ChangeCart, async () => {
        console.log(StorageEvent.ChangeCart);
        await this.updateCartFromIDB();
      });
      idb.on(StorageEvent.AddCart, async () => {
        console.log(StorageEvent.AddCart);
        await this.updateCartFromIDB();
      });
      idb.on(StorageEvent.DeleteCart, async () => {
        console.log(StorageEvent.DeleteCart);
        await this.updateCartFromIDB();
      });
    },

    async syncCarts() {
      async function processCartResponse(
        resp,
        options?: {
          provider_cart_id?: number;
          provider_id?: number;
          local_id?: number;
        },
      ): Promise<any> {
        const { provider_cart_id, provider_id, local_id } = options;
        resp.data.provider = {
          cart: {
            is_synced: true,
            is_deleted: false,
            id: provider_cart_id || resp.data.id.toString(),
          },
          id: provider_id || 1,
        };
        delete resp.data.id;
        resp.data.local_id = local_id || null;

        await idb.saveOrUpdateCartByProvider(resp.data);
      }

      try {
        const resp = await backend.get<Cart[]>('/api/carts');
        handleApiResponse(resp, async () => {
          for (const cart of resp.data) {
            cart.provider = {
              cart: { id: cart.id, is_synced: true, is_deleted: false },
              id: 1,
            };
            delete cart.id;
            await idb.saveOrUpdateCartByProvider(cart);
          }
        });
        const carts = await idb.getAllCarts();
        const cartsToDelete = carts.filter(
          (cart) => cart.provider.cart.is_deleted,
        );
        const otherCarts = carts.filter(
          (cart) => !cart.provider.cart.is_deleted,
        );
        const cartsToAdd = otherCarts.filter(
          (cart) =>
            !cart.provider.cart.is_synced && cart.provider.cart.id === null,
        );
        const cartsToUpdate = otherCarts.filter(
          (cart) =>
            !cart.provider.cart.is_synced && cart.provider.cart.id !== null,
        );

        for (const cart of cartsToDelete) {
          const resp = await backend.delete<Cart>(
            '/api/carts/' + cart.provider.cart.id,
          );

          handleApiResponse(
            resp,
            async () => await idb.deleteCart(cart.local_id),
          );
        }

        for (const cart of cartsToAdd) {
          const local_id = cart.local_id;
          delete cart.provider;
          delete cart.local_id;
          const resp = await backend.post<Cart>('/api/carts', { ...cart });
          handleApiResponse(
            resp,
            async () => await processCartResponse(resp, { local_id }),
          );
        }

        for (const cart of cartsToUpdate) {
          const provider_cart_id = cart.provider.cart.id;
          const provider_id = cart.provider.id;
          const local_id = cart.local_id;
          delete cart.provider;
          delete cart.local_id;
          cart.id = provider_cart_id;

          const resp = await backend.put<Cart>(
            '/api/carts/' + provider_cart_id,
            { ...cart },
          );
          handleApiResponse(
            resp,
            async () =>
              await processCartResponse(resp, {
                provider_cart_id,
                provider_id,
                local_id,
              }),
          );
        }
      } catch (error) {
        handleApiError(error);
      }
    },

    // Получение списка корзин
    async fetchCarts(): Promise<void> {
      try {
        await this.checkOnlineStatus();
        const resp = await backend.get<Cart[]>('/api/carts');
        handleApiResponse(resp, async () => {
          for (const cart of resp.data) {
            cart.provider.cart.id = cart.id;
            delete cart.id;
            cart.provider.id = 1;

            await idb.saveOrUpdateCartByProvider(cart);
          }
        });
      } catch (error) {
        handleApiError(error);
      }
    },
    // Добавление новой корзины
    async addCart(cart: Cart): Promise<Cart> {
      const newCart = await idb.saveCart(cart);
      await idb.setCartSynced(newCart.local_id, false);
      return newCart;
    },
    // Редактирование существующей корзины
    async editCart(cart: Cart): Promise<void> {
      await idb.saveCart(cart);
      await idb.setCartSynced(cart.local_id, false);
    },

    // Удаление корзины
    async deleteCart(cartId: number): Promise<void> {
      const cart = await idb.getCart(cartId);
      if (cart.provider.cart.id) {
        await idb.setCartDeleted(cartId, true);
        await idb.setCartSynced(cartId, false);
      } else {
        await idb.deleteCart(cartId);
      }
    },

    // Действие для добавления нового товара в корзину
    async onAddItem(
      cartId: number,
      text: string,
      quantity_plan: number,
    ): Promise<void> {
      return await idb.getCart(cartId).then(async (cart: Cart) => {
        const cartCopy = { ...cart };
        const items = cartCopy.items;
        function nextId(items) {
          const sortedIds = items.map((item) => item.id).sort((a, b) => a - b);

          let nextId = 1;
          for (const id of sortedIds) {
            if (id !== nextId) {
              return nextId; // Найдена "дыра" в идентификаторах
            }
            nextId++;
          }

          return nextId;
        }

        cartCopy.items.push({
          name: text,
          quantity_plan: quantity_plan,
          quantity_fact: 0,
          id: nextId(items),
          picked: false,
        });
        const newCart = await idb.saveCart(cartCopy);
        await idb.setCartSynced(newCart.local_id, false);
      });
    },

    // Действие для переключения состояния товара (выбрано/не выбрано)
    async onToggleItem(
      cartId: number,
      itemId: number,
      picked: boolean,
    ): Promise<any> {
      await idb.updateItemInCart(cartId, itemId, { picked });
      await idb.setCartSynced(cartId, false);

      return Promise.resolve({ picked });
    },

    // Действие для удаления товара из корзины
    async onDelItem(cartId: number, itemId: number): Promise<void> {
      await idb.removeItemFromCart(cartId, itemId);
      await idb.setCartSynced(cartId, false);
    },

    // Действие для редактирования товара в корзине
    async onEditItem(
      cartId: number,
      itemId: number,
      updatedFields: Partial<Item>,
    ): Promise<any> {
      await idb.updateItemInCart(cartId, itemId, updatedFields);
      await idb.setCartSynced(cartId, false);
    },
  },
});

// Поддержка горячего обновления модуля для разработки
//@ts-ignore
if (import.meta.hot) {
  //@ts-ignore
  import.meta.hot.accept(acceptHMRUpdate(useAllCartsStore, import.meta.hot));
}
