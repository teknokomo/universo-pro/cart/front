// Импорт функции precacheAndRoute из библиотеки workbox-precaching для предварительного кеширования.
import { precacheAndRoute } from 'workbox-precaching';

// Импорт функции registerRoute из библиотеки workbox-routing для управления маршрутизацией запросов.
import { registerRoute } from 'workbox-routing';

// Импорт стратегий кеширования из библиотеки workbox-strategies.
import {
  NetworkFirst,
  NetworkOnly,
  StaleWhileRevalidate,
  CacheFirst,
} from 'workbox-strategies';

// Импорт BroadcastUpdatePlugin для отправки уведомлений о обновлениях кешированных ресурсов.
import { BroadcastUpdatePlugin } from 'workbox-broadcast-update';
import axios from 'axios';

// Определение версии сервисворкера.
const version = '1.0.2';

// Вывод текущей версии сервисворкера в консоль.
console.log(version);
const backend = axios.create({
  withCredentials: true,
  headers: { 'Content-Type': 'application/json' },
});
// Функция для преобразования заголовков HTTP-ответа в объект.
const headersToObject = (headers) => Object.fromEntries(headers.entries());

// Создание экземпляра BroadcastUpdatePlugin с настройками.
const broadcastUpdatePlugin = new BroadcastUpdatePlugin({
  headersToCheck: ['etag', 'last-modified'], // Заголовки для проверки изменений.
  generatePayload: ({ cacheName, request, oldResponse, newResponse }) => ({
    cacheName,
    updatedURL: request.url,
    oldHeaders: headersToObject(oldResponse?.headers),
    newHeaders: headersToObject(newResponse?.headers),
  }),
});

// Предварительное кеширование ресурсов, определённых в манифесте сервисворкера.
//@ts-ignore
precacheAndRoute(self.__WB_MANIFEST);

// Определение текущего состояния подключения к интернету.
let isOnline = navigator.onLine;

// Слушатель события online для обновления статуса подключения.
self.addEventListener('online', () => {
  isOnline = true;
  console.log('Подключено к интернету');
});

// Слушатель события offline для обновления статуса подключения.
self.addEventListener('offline', () => {
  isOnline = false;
  console.log('Отключено от интернета');
});

// Асинхронная функция активации сервисворкера.
const activateServiceWorker = async () => {
  //@ts-ignore
  const skipWaiting = await self.skipWaiting(); // Пропуск ожидания.
  console.log('Пропуск ожидания: ', skipWaiting);
  //@ts-ignore
  const clientsClaim = await self.clients.claim(); // Захват управления клиентами.
  console.log('Захват управления клиентами: ', clientsClaim);
  //@ts-ignore
  const registrationUpdate = await self.registration.update(); // Обновление регистрации.
  console.log('Обновление регистрации: ', registrationUpdate);
  //@ts-ignore
  const clients = await self.clients.matchAll({ type: 'window' });
  console.log('Клиенты: ', clients);
  clients.forEach((client) => client.postMessage('new-version-available')); // Оповещение клиентов о новой версии.
};

// Слушатель события установки сервисворкера.
self.addEventListener('install', (event) => {
  console.log('Слушатель события активации сервисворкера install: ', event);
  //@ts-ignore
  return event.waitUntil(self.skipWaiting());
});

// Слушатель события активации сервисворкера.
self.addEventListener('push', (event) =>
  console.log('Слушатель события активации сервисворкера push: ', event),
);

// Слушатель события активации сервисворкера.
self.addEventListener('activate', (event) => {
  console.log('Слушатель события активации сервисворкера activate: ', event);
  //@ts-ignore
  return event.waitUntil(activateServiceWorker());
});

// Функция для удаления параметра authenticated из URL.
const removeAuthenticatedParam = (url) => {
  if (url.searchParams.has('authenticated')) {
    url.searchParams.delete('authenticated');
  }
};

// Функция для регистрации маршрута с BroadcastUpdatePlugin.
const registerBroadcastUpdateRoute = (matchFunction, strategy) => {
  registerRoute(
    (args) => {
      const url = new URL(args.url.href);
      removeAuthenticatedParam(url); // Очистка параметра authenticated.
      return matchFunction(args);
    },
    new strategy({ plugins: [broadcastUpdatePlugin] }), // Использование выбранной стратегии с плагином.
  );
};

// Функция определения запроса аутентификации.
const isAuthenticationRequest = ({ url }) =>
  url.href.includes('?authenticated');

// Функция определения запроса статических ресурсов.
const isStaticResource = (request) =>
  ['script', 'style', 'worker', 'image'].includes(request.destination);

// Функция определения запроса к бэкенду или OAuth.
const isBackendOrOauthRequest = ({ href }) => {
  const result =
    href.includes('/universo/oauth/v2/') ||
    href.includes('/user') ||
    href.includes('/api/');
  console.log(href, ' : ', result);
  return result;
};
// Слушатель события fetch (перехват запросов).
// self.addEventListener('fetch', (event) => {
//   //@ts-ignore

//   const url = new URL(event.request.url);
//   removeAuthenticatedParam(url); // Очистка параметра authenticated.
//   //@ts-ignore
//   event.respondWith(fetch(event.request)); // Обработка сетевого запроса без кеширования.
//   return;
//   // // Проверка, является ли запрос запросом к бэкенду.
//   // if (isBackendOrOauthRequest(url)) {
//   //   //@ts-ignore

//   //   event.respondWith(fetch(event.request)); // Обработка сетевого запроса без кеширования.
//   //   return;
//   // }

//   // // // Выбор стратегии кеширования в зависимости от состояния подключения.
//   // // const strategy = isOnline ? new NetworkFirst() : new CacheFirst();
//   // //@ts-ignore

//   // event.respondWith(
//   //   caches
//   //     .match(url)
//   //     .then((cachedResponse) => cachedResponse || fetch(url)) // Попытка получить ответ из кеша или выполнить запрос.
//   //     .catch(() => caches.match('/')), // В случае ошибки пытается загрузить корневую страницу.
//   // );
// });
// Регистрация маршрутов с различными стратегиями и BroadcastUpdatePlugin.
registerBroadcastUpdateRoute(isAuthenticationRequest, NetworkOnly);
registerBroadcastUpdateRoute(
  ({ request }) => isStaticResource(request),
  CacheFirst,
);
registerBroadcastUpdateRoute(({ url }) => {
  return isBackendOrOauthRequest(url);
}, NetworkOnly);
registerBroadcastUpdateRoute(
  (args) =>
    !isStaticResource(args.request) && !isBackendOrOauthRequest(args.url),
  StaleWhileRevalidate,
);
// registerBroadcastUpdateRoute(/api\/data/, StaleWhileRevalidate);
