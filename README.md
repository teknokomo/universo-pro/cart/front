# Список покупок (learn-front)

Приложение для планирования и совместного ведения списка покупок.
Используется повседневно.

# Запуск на машине разработчика

## Установить глобально YARN и Quasar CLI

```bash
npm install -g yarn @quasar/cli
```

## Установить зависимости проекта

```bash
yarn
```

## Запустить приложение в режиме разработки (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

### Lint the files

```bash
yarn lint
```

### Format the files

```bash
yarn format
```

# Сборка приложения для использования его на боевом сервере

```bash
quasar build
```

При сборке в корневой директории появится папка dist, в которую Quasar поместит все собранные файлы.

# Конфигурирование настроек Quasar

See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-webpack/quasar-config-js).

# Описание компонентов Quasar

https://www.youtube.com/playlist?list=PLFZAa7EupbB7xC-C0YwYk7aXIAbHYX1Xl
